import shelve
import typing as tp

from distributionhelper.worker.worker import BrowserWorker
from distributionhelper.social_manager.interfaces import ISocial

import logging
logger = logging.getLogger(__name__)

from pathlib import Path
STORAGE_PATH = (Path(__file__).parent / 'storage').absolute()


def get_worker(social: ISocial, browser_type: str = 'chrome', **kwargs) -> BrowserWorker:
    worker = BrowserWorker(social)
    worker.initialize_webdriver(browser_type, **kwargs)
    return worker


def get_workers(socials: tp.List[ISocial]) -> tp.List[BrowserWorker]:
    return [get_worker(s) for s in socials]


def save_worker_cookie(worker: BrowserWorker):
    filename = str(STORAGE_PATH / str(worker.social))
    logger.info(f"Saving cookies for {worker.social} in file {filename}")
    with shelve.open(filename) as d:
        d['cookies'] = worker.get_cookies()


def restore_worker_cookie(worker: BrowserWorker):
    filename = str(STORAGE_PATH / str(worker.social))
    logger.info(f"Restoring cookies for {worker.social} in file {filename}")
    with shelve.open(filename) as d:
        cookies = d.get('cookies', [])
        logger.debug(f"{cookies}")
        worker.set_cookies(cookies)


def save_workers_states(workers: tp.List[BrowserWorker]):
    for worker in workers:
        save_worker_cookie(worker)


def restore_workers_states(workers: tp.List[BrowserWorker]):
    for worker in workers:
        restore_worker_cookie(worker)
