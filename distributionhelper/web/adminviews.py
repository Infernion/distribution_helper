import deform
import colander
from pyramid.view import view_config

from websauna.system.admin import views as defaultadminviews
from websauna.system.admin.modeladmin import ModelAdmin
from websauna.system.crud.views import FormView, TraverseLinkButton, Show
from websauna.system.crud.formgenerator import SQLAlchemyFormGenerator, default_schema_binder
from websauna.system.core.viewconfig import view_overrides
from websauna.system.form.csrf import add_csrf
from websauna.system.form.editmode import EditMode
from websauna.system.form.resourceregistry import ResourceRegistry

from websauna.system.form.fields import UUID  # Custom widget for UUID types
from websauna.system.form.widgets import FriendlyUUIDWidget  # Custom widget for UUID types

from distributionhelper.models import Worker, Campaign, Recipient, Message, MessageDelivery, Account
from . import admins


class CampaignAdminFormViewMixin(FormView):
    def create_form(self, mode: EditMode, buttons=(), nested=None) -> deform.Form:
        model = self.get_model()

        class Workers(colander.SequenceSchema):
            worker = colander.Schema(
                colander.SchemaNode(colander.String(), name='browser'),
                colander.SchemaNode(colander.String(), name='type'),
                colander.SchemaNode(colander.String(), name='availability'),
                colander.SchemaNode(colander.String(), name='ip_address'),
            )

        class Recipients(colander.SequenceSchema):
            recipient = colander.Schema(
                colander.SchemaNode(colander.String(), name='first_name'),
                colander.SchemaNode(colander.String(), name='last_name'),
                colander.SchemaNode(colander.String(), name='profile_url'),
                colander.SchemaNode(colander.String(), name='avatar_url'),
                colander.SchemaNode(colander.String(), name='social_network'),
            )

        class Accounts(colander.SequenceSchema):
            account = colander.Schema(
                colander.SchemaNode(colander.String(), name='username'),
                colander.SchemaNode(colander.String(), name='profile_url'),
                colander.SchemaNode(colander.String(), name='social_network'),
            )

        class Messages(colander.SequenceSchema):
            message = colander.Schema(
                colander.SchemaNode(colander.String(), name='text'),
                colander.SchemaNode(colander.String(), name='attachment'),
            )

        class MessageDeliveries(colander.SequenceSchema):
            message_delivery = colander.Schema(
                colander.SchemaNode(colander.String(), name='status'),
                colander.SchemaNode(colander.String(), name='message_id'),
                colander.SchemaNode(colander.String(), name='recipient_id'),
            )

        class Schema(colander.Schema):
            name = colander.SchemaNode(colander.String())
            description = colander.SchemaNode(colander.String())
            owner = colander.SchemaNode(colander.String())
            social_network = colander.SchemaNode(colander.String())
            status = colander.SchemaNode(colander.String())
            workers = Workers(
                widget=deform.widget.SequenceWidget(orderable=True))
            recipients = Recipients(
                widget=deform.widget.SequenceWidget(orderable=True)
            )
            accounts = Accounts(
                widget=deform.widget.SequenceWidget(orderable=True)
            )
            messages = Messages(
                widget=deform.widget.SequenceWidget(orderable=True)
            )
        schema = Schema()
        return self.form_generator.create_deform(schema, self.request, self.context, mode, buttons, model)

    def get_appstruct(self, form, campaign) -> dict:
        """Get the dictionary that populates the form."""
        workers = [{key: value for key, value in w.__dict__.items() if not key.startswith('_')}
                   for w in campaign.workers]
        recipients = [{key: value for key, value in r.__dict__.items() if not key.startswith('_')}
                      for r in campaign.recipients]
        accounts = [{key: value for key, value in a.__dict__.items() if not key.startswith('_')}
                    for a in campaign.accounts]
        messages = [{key: value for key, value in m.__dict__.items() if not key.startswith('_')}
                    for m in campaign.messages]
        return dict(name=campaign.name,
                    description=campaign.description,
                    created_at=campaign.created_at,
                    updated_at=campaign.updated_at,
                    owner=campaign.owner,
                    status=campaign.status,
                    social_network=campaign.social_network,
                    workers=workers,
                    recipients=recipients,
                    accounts=accounts,
                    messages=messages)


@view_overrides(context=admins.CampaignAdmin.Resource)
class CampaignAdminShow(defaultadminviews.Show):
    resource_buttons = [
                           TraverseLinkButton(id="workers", name="Workers", view_name="workers", permission="view"),
                       ] + defaultadminviews.Show.resource_buttons

    @view_config(context=ModelAdmin.Resource, name="show", renderer="admin/campaign_show.html", route_name="admin",
                 permission='view')
    def show(self):
        context = super().show()
        campaign = self.context.get_object()
        return dict(campaign=campaign, **context)


@view_overrides(context=admins.CampaignAdmin.Resource)
class CampaignAdminEdit(CampaignAdminFormViewMixin, defaultadminviews.Edit):
    def save_changes(self, form: deform.Form, appstruct: dict, obj: object):
        """Store the data from the form on the object."""
        dbsession = self.context.request.dbsession
        campaign = Campaign(name=appstruct['name'], description=appstruct['description'],
                            social_network=appstruct['social_network'])

        workers = []
        for worker in appstruct['workers']:
            worker = Worker(browser=worker['browser'], type=worker['type'],
                            availability=worker['availability'], ip_address=worker['ip_address'])
            dbsession.add(worker)
            workers.append(worker)

        campaign.workers.append(worker)

        return campaign
