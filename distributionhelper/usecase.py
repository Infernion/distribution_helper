class UseCase:
    pass

class GetWorkersByStatus(UseCase):

    def get_avaliable_workers(self):
        pass

    def get_bussy_workers(self):
        pass


class BindWorkers(UseCase):

    def bind_worker(self, worker, campaign):
        pass

    def bind_workers(self, workers, campaing):
        pass
