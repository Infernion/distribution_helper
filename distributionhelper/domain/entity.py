import typing as tp
from typing import NamedTuple


class Account(NamedTuple):
    username: str
    password: str
    profile_url: str
    social_networks: list


class Campaign(NamedTuple):
    name: str


class Attachement(NamedTuple):
    kind: str
    body: str


class Tag(NamedTuple):
    name: str


class Message(NamedTuple):
    text: str


class Recipient(NamedTuple):
    first_name: str
    last_name: str
    profile_url: str
    avatar_url: str
    social_network: list


class SocialNetwork(NamedTuple):
    name: str
    login_url: str


class SocialNetworkAccess(NamedTuple):
    social_network: dict
    account: dict
    cookies: tp.List[dict]
    cookies: list


class User(NamedTuple):
    first_name: str
    last_name: str
    password: str
    email: str


class BrowserWorker(NamedTuple):
    browser: str
    avalability: str
    ipv4_address: str


class Action(NamedTuple):
    name: str
    args: tp.Optional[tp.Tuple[tp.AnyStr]]
